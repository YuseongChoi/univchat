﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;

namespace UnivChatData {
    [Serializable]
    public class ChatObject : INotifyPropertyChanged {
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name) {
            if (this.PropertyChanged is null) return;

            this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }

    [Serializable]
    public class User : ChatObject {
        #region Properties
        public string ID {
            get => this.id;
            set {
                this.id = value;
                this.OnPropertyChanged(nameof(this.ID));
            }
        }
        public string Profile {
            get => this.profile;
            set {
                this.profile = value;
                this.OnPropertyChanged(nameof(this.Profile));
            }
        }
        public string Name {
            get => this.name;
            set {
                this.name = value;
                this.OnPropertyChanged(nameof(this.Name));
            }
        }
        public bool IsOnline {
            get => this.isonline;
            set {
                this.isonline = value;
                this.OnPropertyChanged(nameof(this.IsOnline));
            }
        }
        #endregion

        private string id;
        private string profile;
        private string name;
        private bool isonline;
    }

    [Serializable]
    public class Room : ChatObject {
        #region Properties
        public string Name {
            get => this.name;
            set {
                this.name = value;
                this.OnPropertyChanged(nameof(this.Name));
            }
        }
        public User RoomMaster {
            get => this.roommaster;
            set {
                this.roommaster = value;
                this.OnPropertyChanged(nameof(this.RoomMaster));
            }
        }
        public string RoomNumber {
            get => this.roomnumber;
            set {
                this.roomnumber = value;
                this.OnPropertyChanged(nameof(this.RoomNumber));
            }
        }
        #endregion

        public ObservableCollection<User> Users { get; private set; } = new ObservableCollection<User>();

        private string name;
        private User roommaster;
        private string roomnumber;

        private object locker = new object();

        public IEnumerator<Chat> GetChatsi() {
            string path = $"./Chats/{this.roomnumber}.txt";

            IEnumerable<string> all = null;

            lock(this.locker) {
                try {
                    all = File.ReadAllLines(path).Reverse();
                }
                catch (FileNotFoundException) {
                    File.Create(path);
                }
                catch (DirectoryNotFoundException) {
                    Directory.CreateDirectory("./Chats");
                    File.Create(path);
                }
                catch (IOException) {

                }
            }

            if (all is null) yield break;

            foreach (string text in all) {
                Chat chat = Chat.ToChat(text);
                yield return chat;
            }
        }

        public void AppendLog(Chat chat) {
            string path = this.RoomNumber;

            lock (this.locker) {
                try {
                    using (FileStream fs = new FileStream($"./Chats/{path}.txt", FileMode.Append, FileAccess.Write)) {
                        using (TextWriter tw = new StreamWriter(fs)) {
                            tw.WriteLine($"{chat.ToString()}");
                            tw.Flush();
                        }
                    }
                }
                catch(IOException) {

                }
            }
        } // END OF AppendLog Method
    }

    [Serializable]
    public class Chat : ChatObject {
        #region Properties
        public string Text {
            get => this.text;
            set {
                this.text = value;
                this.OnPropertyChanged(nameof(this.Text));
            }
        }
        public DateTime Date {
            get => this.date;
            set {
                this.date = value;
                this.OnPropertyChanged(nameof(this.Date));
            }
        }
        public User User {
            get => this.user;
            set {
                this.user = value;
                this.OnPropertyChanged(nameof(this.User));
            }
        }
        #endregion

        private string text;
        private DateTime date;
        private User user;

        public override string ToString() {
            return $"{date}\t{User.ID}\t{text}";
        }

        public static Chat ToChat(string text) {
            text = text.TrimEnd('\n', '\r');

            String[] tokens = text.Split('\t');

            if (tokens.Length <= 2 || tokens.Length > 3) return null;

            DateTime date = DateTime.Parse(tokens[0]);
            User user = new User() { ID = tokens[1] };
            Chat chat = new Chat() { text = tokens[2], Date = date, User = user };

            return chat;
        }
    }
}
