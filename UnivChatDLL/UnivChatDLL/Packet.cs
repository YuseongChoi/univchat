﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using System.Xml.Serialization;
using System.Xml;

using UnivChatData;

namespace UnivChatPacket {
    [Serializable]
    abstract public class Packet {
        public static Packet[] GetPacket(string xml) {
            List<Packet> packets = new List<Packet>();

            Regex regex = new Regex(@"<\?xml.*?\?>", RegexOptions.Singleline);

            xml = regex.Replace(xml, "");
            xml = $"<Root>{xml}</Root>";

            XmlDocument doc = new XmlDocument();
            try {
                doc.LoadXml(xml);
            }
            catch(XmlException) {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("----------------ERROR:----------------");
                System.Diagnostics.Debug.WriteLine(xml);
                System.Diagnostics.Debug.WriteLine("--------------------------------------");
#endif
                return packets.ToArray();
            }
            
            foreach (XmlNode node in doc.DocumentElement.ChildNodes) {
                Type stype = Type.GetType("UnivChatPacket." + node.Name);
                if (stype is null) continue;

                Packet packet = null;

                XmlSerializer serializer = new XmlSerializer(stype);
                using (XmlReader reader = new XmlNodeReader(node)) {
                    try {
                        packet = serializer.Deserialize(reader) as Packet;
                        packets.Add(packet);
                    }
                    catch (InvalidOperationException e) {
                        System.Diagnostics.Debug.WriteLine($"{e.GetType().ToString()} : XML SERIALIZE FAILED");
                        continue;
                    }
                }
            }
            
            return packets.ToArray();
        }

        public virtual byte[] GetXML() {
            byte[] result;
            
            StringBuilder sb = new StringBuilder();

            using (StringWriter sw = new StringWriter(sb)) {
                XmlSerializer serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(sw, this);
                sw.Flush();
            }
            
            result = Encoding.UTF8.GetBytes(sb.ToString());

            return result;
        }
    }

    [Serializable]
    public class Login : Packet {
        [XmlElement(nameof(UserID))]
        public string UserID;
        [XmlElement(nameof(UserPassword))]
        public string UserPassword;
    }

    [Serializable]
    public class LoginResponse : Packet {
        [XmlElement(nameof(IsSuccess))]
        public bool IsSuccess;
        [XmlElement(nameof(UserName))]
        public string UserName;
        [XmlElement(nameof(ProfileText))]
        public string ProfileText;
    }

    [Serializable]
    public class RoomInfo : Packet {
        [XmlElement(nameof(RoomNumber))]
        public string RoomNumber;
        [XmlElement(nameof(RoomName))]
        public string RoomName;
        [XmlElement(nameof(RoomMasterID))]
        public string RoomMasterID;
    }

    [Serializable]
    public class FriendInfo : Packet {
        [XmlElement(nameof(FriendID))]
        public string FriendID;
        [XmlElement(nameof(UserName))]
        public string UserName;
        [XmlElement(nameof(ProfileText))]
        public string ProfileText;
    }

    [Serializable]
    public class AddChatting : Packet {
        [XmlElement(nameof(WriteUserID))]
        public string WriteUserID;
        [XmlElement(nameof(RoomNumber))]
        public string RoomNumber;
        [XmlElement(nameof(Date))]
        public DateTime Date;
        [XmlElement(nameof(CharText))]
        public string CharText;
    }

    [Serializable]
    public class AddUserInRoom : Packet {
        [XmlElement(nameof(RoomNumber))]
        public string RoomNumber;
        [XmlElement(nameof(AddUserID))]
        public string AddUserID;
        [XmlElement(nameof(UserName))]
        public string UserName;
        [XmlElement(nameof(ProfileText))]
        public string ProfileText;
        [XmlElement(nameof(IsOnline))]
        public bool IsOnline;
    }

    [Serializable]
    public class FindUser : Packet {
        [XmlElement(nameof(UserID))]
        public string UserID;
    }

    [Serializable]
    public class FindUserResponse : Packet {
        [XmlElement(nameof(UserID))]
        public string UserID;
        [XmlElement(nameof(UserName))]
        public string UserName;
        [XmlElement(nameof(ProfileText))]
        public string ProfileText;
    }

    [Serializable]
    public class AddFriendRequest : Packet {
        [XmlElement(nameof(FriendID))]
        public string FriendID;
        [XmlElement(nameof(IsMyRequest))]
        public bool IsMyRequest;
    }

    [Serializable]
    public class AddFriendResponse : Packet {
        [XmlElement(nameof(RequestUserID))]
        public string RequestUserID;
        [XmlElement(nameof(IsAccept))]
        public bool IsAccept;
    }

    [Serializable]
    public class CreateAccount : Packet {
        [XmlElement(nameof(UserID))]
        public string UserID;
        [XmlElement(nameof(UserPassword))]
        public string UserPassword;
        [XmlElement(nameof(UserName))]
        public string UserName;
        [XmlElement(nameof(ProfileText))]
        public string ProfileText;
    }

    [Serializable]
    public class ChangeName : Packet {
        [XmlElement(nameof(UserName))]
        public string UserName;
    }

    [Serializable]
    public class ChangeProfile : Packet {
        [XmlElement(nameof(UserProfile))]
        public string UserProfile;
    }
}
