﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using UnivChatData;
using UnivChatPacket;

namespace TestServer {
    class Server {
        static private Server sys = new Server();
        static public Server System { get => Server.sys; }

        private TcpListener server = null;
        private IPEndPoint IPEndPoint;

        public int ServerPort = 7001;

        private Server() {
            IPAddress iPAddress = IPAddress.Any;
            this.IPEndPoint = new IPEndPoint(iPAddress, this.ServerPort);
        }

        ~Server() {

        }
        
        public void AcceptClient() {
            this.server = new TcpListener(this.IPEndPoint);
            this.server.Start();

            while(true) {
                TcpClient client = this.server.AcceptTcpClient();
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine($"CONNECT IP : {((IPEndPoint)client.Client.RemoteEndPoint).ToString()}");
                Console.WriteLine("----------------------------------------------");

                Task.Run(() => { this.RunClient(client); });
            }
        }

        public void RunClient(TcpClient client) {
            string adress = ((IPEndPoint)client.Client.RemoteEndPoint).ToString();

            byte[] output = new byte[client.SendBufferSize];

            NetworkStream stream = client.GetStream();

            ///////////////////////
            //// PACKET ONCE
            ////////////////////////
            for (int i = 0; i < 10; i++) {
                AddFriendRequest fr = new AddFriendRequest() { FriendID = $"ididi{i}" };
                byte[] send = fr.GetXML();

                stream.Write(send, 0, send.Length);
            }
            //////////////////////////

            try {
                while (client.Connected) {
                    int count = stream.Read(output, 0, output.Length);
                    Console.WriteLine($"---------------{adress} Something Received-----------------");

                    string xml = Encoding.UTF8.GetString(output, 0, count);
                    Console.WriteLine(xml);

                    Packet[] packets = Packet.GetPacket(xml);

                    foreach (Packet packet in packets) {
                        this.Handle(stream, packet);
                    }
                }
            }
            catch(IOException e) {
                Console.WriteLine($"{adress} Is DEAD");
                return;
            }
        }

        public void Handle(NetworkStream stream, Packet packet) {
            if (packet is null) return;
            
            if (packet is Login login) {
                Console.WriteLine($"ID: {login.UserID} PW: {login.UserPassword}");

                LoginResponse response = new LoginResponse() { IsSuccess = true, UserName = login.UserID, ProfileText = login.UserID };

                byte[] send = response.GetXML();
                stream.Write(send, 0, send.Length);
            }
            else if (packet is AddChatting addchatting) {
                Console.WriteLine($"ID: {addchatting.WriteUserID} Room: {addchatting.RoomNumber} Text: {addchatting.CharText}");

                addchatting.Date = DateTime.Now;

                byte[] send = addchatting.GetXML();
                stream.Write(send, 0, send.Length);
            }
            else if (packet is AddUserInRoom adduserinroom) {

                byte[] send = adduserinroom.GetXML();
                stream.Write(send, 0, send.Length);
            }
            else if (packet is AddFriendResponse fr) {
                if (fr.IsAccept) {
                    FriendInfo info = new FriendInfo() { FriendID = fr.RequestUserID, UserName = fr.RequestUserID, ProfileText = "" };

                    byte[] send = info.GetXML();

                    stream.Write(send, 0, send.Length);
                }
            }
            else if (packet is ChangeProfile cp) {
                byte[] send = cp.GetXML();

                stream.Write(send, 0, send.Length);
            }
            else if (packet is CreateAccount ca) {
                LoginResponse lr = new LoginResponse() { IsSuccess = true, UserName = ca.UserName, ProfileText = ca.ProfileText };

                byte[] send = lr.GetXML();

                stream.Write(send, 0, send.Length);
            }
            else if (packet is RoomInfo ar) {
                byte[] send = ar.GetXML();

                stream.Write(send, 0, send.Length);
            }
            else if (packet is FindUser fu) {
                FindUserResponse fur = new FindUserResponse() { UserID = fu.UserID, UserName = fu.UserID, ProfileText = fu.UserID };

                byte[] send = fur.GetXML();

                stream.Write(send, 0, send.Length);
            }
        }
    }

    class Program {
        static void Main(string[] args) {
            Console.WriteLine("============= TEST SERVER ================");
            Server.System.AcceptClient();
        }
    }
}
