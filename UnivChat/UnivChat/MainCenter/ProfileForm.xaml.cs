﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UnivChatData;
using UnivChatPacket;

namespace UnivChat
{
    /// <summary>
    /// ProfileForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ProfileForm : UserControl {
        public ProfileForm() {
            InitializeComponent();
        }

        /// <summary>
        /// 폼 로드 했을 때
        /// </summary>
        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            Data.GetData.Users = Data.GetData.Friends;
            Data.GetData.CurrentRoom = null;

            this.Profile_TextBlock.Text = Data.GetData.Me.Profile;
        }

        /// <summary>
        /// 수락 버튼 클릭
        /// </summary>
        private void Button_Click(object sender, RoutedEventArgs e) {
            Button button = sender as Button;
            User user = button.DataContext as User;
            if (user is null) return;

            AddFriendResponse response = new AddFriendResponse() { IsAccept = true, RequestUserID = user.ID };
            Network.GetInstance.SendPacket(response);

            Data.GetData.FriendsRequests.Remove(user);
        }

        /// <summary>
        /// 거절 버튼 클릭
        /// </summary>
        private void Button_Click_1(object sender, RoutedEventArgs e) {
            Button button = sender as Button;
            User user = button.DataContext as User;
            if (user is null) return;

            AddFriendResponse response = new AddFriendResponse() { IsAccept = false, RequestUserID = user.ID };
            Network.GetInstance.SendPacket(response);

            Data.GetData.FriendsRequests.Remove(user);
        }

        /// <summary>
        /// 프로필 텍스트 블록에서 키 눌렀을 때
        /// </summary>
        private void Profile_TextBlock_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key is Key.Enter) {
                string profile = this.Profile_TextBlock.Text;
                ChangeProfile cp = new ChangeProfile() { UserProfile = profile };

                Network.GetInstance.SendPacket(cp);
            }
        }
    }
}
