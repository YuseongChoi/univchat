﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using UnivChatData;
using UnivChatPacket;

namespace UnivChat
{
    /// <summary>
    /// ChatForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ChatForm : UserControl {
        private bool autoscroll = true;
        IEnumerator<Chat> chats = null;

        private double scrollable = 0;

        public ChatForm() {
            InitializeComponent();

            ItemsControl chatitems = this.ChatViewer;

            chatitems.Items.SortDescriptions.Clear();
            chatitems.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription("Date", System.ComponentModel.ListSortDirection.Ascending)
            );
        }

        /// <summary>
        /// 채팅 보내기
        /// </summary>
        private void SendChat() {
            string text = this.ChatText.Text;
            if (String.IsNullOrWhiteSpace(text)) return;

            Chat chat = new Chat() { Text = text, User = Data.GetData.Me };
            AddChatting add = new AddChatting() {
                RoomNumber = Data.GetData.CurrentRoom.RoomNumber,
                WriteUserID = Data.GetData.Me.ID,
                CharText = chat.Text
            };
            Network.GetInstance.SendPacket(add);

            this.ChatText.Text = "";
        }

        /// <summary>
        /// 채팅 보내기 버튼 클릭
        /// </summary>
        private void Button_Click(object sender, RoutedEventArgs e) {
            this.SendChat();
        }

        /// <summary>
        /// 채팅 박스 포커스일 시 키보드 버튼 다운
        /// </summary>
        private void ChatText_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                this.SendChat();
            }
        }


        /// <summary>
        /// 오토 스크롤
        /// </summary>
        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e) {
            if (e.ExtentHeightChange == 0) {
                if (this.ChatScroll.VerticalOffset == this.ChatScroll.ScrollableHeight) {
                    this.autoscroll = true;
                }
                else {
                    this.autoscroll = false;
                }
            }

            if (this.autoscroll && e.ExtentHeightChange != 0) {
                this.ChatScroll.ScrollToVerticalOffset(this.ChatScroll.ExtentHeight);
            }

            if (this.ChatScroll.VerticalOffset is 0 && e.VerticalChange != 0) {
                this.LoadChats();
            }
        }

        /// <summary>
        /// 폼이 로드 되었을 때
        /// </summary>
        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            this.chats = Data.GetData.CurrentRoom.GetChatsi();

            this.LoadChats();
        }

        /// <summary>
        /// 반복자에서 Chat 정보 취득
        /// </summary>
        private void LoadChats() {
            if (this.chats is null) return;

            for (int i = 0; i < 50; i++) {
                if (this.chats.MoveNext()) {
                    Chat chat = this.chats.Current;

                    chat.User = Data.GetData.FindUserFromPool(chat.User.ID);

                    Data.GetData.Chats.Add(chat);
                }
                else break;
            }
        }

        /// <summary>
        /// 컨텐츠 추가되었을 때
        /// </summary>
        private void ChatViewer_SizeChanged(object sender, SizeChangedEventArgs e) {
            double newscroll = this.ChatScroll.ScrollableHeight;

            double delta = newscroll - this.scrollable;

            this.ChatScroll.ScrollToVerticalOffset(delta);

            this.scrollable = newscroll;
        }
    } // END OF ChatForm CLASS
}
