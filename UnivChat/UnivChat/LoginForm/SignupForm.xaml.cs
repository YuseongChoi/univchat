﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Xml.Serialization;
using System.Xml;

using UnivChatData;
using UnivChatPacket;
using System.Net.Sockets;

namespace UnivChat
{
    /// <summary>
    /// LoginForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class SignupForm : UserControl {
        internal MainWindow mainwindow = null;

        public SignupForm() {
            InitializeComponent();
        }

        public void Enable() {
            this.NameBox.IsEnabled = this.IDBox.IsEnabled = this.PasswordBox.IsEnabled = this.SignupButton.IsEnabled = this.CancelButton.IsEnabled = true;
        }

        public void Disable() {
            this.NameBox.IsEnabled = this.IDBox.IsEnabled = this.PasswordBox.IsEnabled = this.SignupButton.IsEnabled = this.CancelButton.IsEnabled = false;
        }

        /// <summary>
        /// 회원가입 버튼 클릭
        /// </summary>
        private async void Button_Click(object sender, RoutedEventArgs e) {
            _ = this.IDBox.Text;
            _ = this.PasswordBox.Text;
            _ = this.NameBox.Text;

            if (String.IsNullOrWhiteSpace(this.IDBox.Text) || 
                String.IsNullOrWhiteSpace(this.PasswordBox.Text) ||
                String.IsNullOrWhiteSpace(this.NameBox.Text)) {
                return;
            }

            Network.GetInstance.ServerIP = this.IPBox.Text;
            try {
                Network.GetInstance.ServerPort = Convert.ToInt32(this.PortBox.Text);
            }
            catch (FormatException) {
                MessageBox.Show("포트 번호가 올바르지 않습니다.");
                return;
            }

            try {
                Network.GetInstance.Connect();
            }
            catch (SocketException) {
                MessageBox.Show("서버에 연결하지 못했습니다.");
                return;
            }
            catch (FormatException) {
                MessageBox.Show("아이피 주소가 올바르지 않습니다.");
                return;
            }

            this.Disable();

            CreateAccount ca = new CreateAccount() {
                UserID = this.IDBox.Text, UserPassword = this.PasswordBox.Text, UserName = this.NameBox.Text, ProfileText = "" };

            Network.GetInstance.SendPacket(ca);
            Packet packet = await Network.GetInstance.WaitToken(typeof(LoginResponse));

            if (packet is LoginResponse lr) {
                if (lr.IsSuccess) {
                    //////////////////
                    Data.GetData.Me = new User() {
                        ID = this.IDBox.Text,
                        Name = lr.UserName,
                        Profile = lr.ProfileText,
                        IsOnline = true
                    };
                    ////////////////
                    this.mainwindow.MainGrid.Children.Clear();
                    this.mainwindow.MainGrid.Children.Add(new MainForm());
                }
            }
            this.Enable();
        } // END OF 

        /// <summary>
        /// 취소 버튼 클릭
        /// </summary>
        private void CancelButton_Click(object sender, RoutedEventArgs e) {
            if (this.mainwindow is null) return;

            this.mainwindow.MainGrid.Children.Clear();

            LoginForm loginForm = new LoginForm();
            loginForm.mainwindow = this.mainwindow;

            this.mainwindow.MainGrid.Children.Add(loginForm);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            this.IPBox.Text = "127.0.0.2";
            this.PortBox.Text = "7001";
        }
    } // END OF SignupForm CLASS
}
