﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Xml.Serialization;
using System.Xml;

using UnivChatData;
using UnivChatPacket;
using System.Net.Sockets;
using System.Net;

namespace UnivChat
{
    /// <summary>
    /// LoginForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class LoginForm : UserControl {
        internal MainWindow mainwindow = null;

        public LoginForm() {
            InitializeComponent();
        }

        public void Enable() {
            this.IDBox.IsEnabled = this.PasswordBox.IsEnabled = this.LoginButton.IsEnabled = this.SignupButton.IsEnabled = true;
        }

        public void Disable() {
            this.IDBox.IsEnabled = this.PasswordBox.IsEnabled = this.LoginButton.IsEnabled = this.SignupButton.IsEnabled = false;
        }

        /// <summary>
        /// 로그인 버튼 클릭
        /// </summary>
        private async void Button_Click(object sender, RoutedEventArgs e) {
            string id = this.IDBox.Text;
            string password = this.PasswordBox.Text;

            if (String.IsNullOrWhiteSpace(id) ||
                String.IsNullOrWhiteSpace(password)) {
                return;
            }

            Network.GetInstance.ServerIP = this.IPBox.Text;


            try {
                Network.GetInstance.ServerPort = Convert.ToInt32(this.PortBox.Text);
            }
            catch(FormatException) {
                MessageBox.Show("포트 번호가 올바르지 않습니다.");
                return;
            }
            
            try {
                Network.GetInstance.Connect();
            }
            catch (SocketException) {
                MessageBox.Show("서버에 연결하지 못했습니다.");
                return;
            }
            catch(FormatException) {
                MessageBox.Show("아이피 주소가 올바르지 않습니다.");
                return;
            }
            

            this.Disable();

            Login login = new Login() { UserID = id, UserPassword = password };

            Network.GetInstance.SendPacket(login);
            Packet packet = await Network.GetInstance.WaitToken(typeof(LoginResponse));

            LoginResponse lr = packet as LoginResponse;
            if (lr is null) return;
            
            if (lr.IsSuccess) {
                //////////////////
                Data.GetData.Me = new User() { 
                    ID = id, Name = lr.UserName, Profile = lr.ProfileText, IsOnline = true
                };
                ////////////////
                this.mainwindow.MainGrid.Children.Clear();
                this.mainwindow.MainGrid.Children.Add(new MainForm());
            }

            this.Enable();
        }

        /// <summary>
        /// 회원가입 버튼 클릭
        /// </summary>
        private void SignupButton_Click(object sender, RoutedEventArgs e) {
            if (this.mainwindow is null) return;
            
            this.mainwindow.MainGrid.Children.Clear();

            SignupForm signupForm = new SignupForm();
            signupForm.mainwindow = this.mainwindow;

            this.mainwindow.MainGrid.Children.Add(signupForm);
        }

        /// <summary>
        /// 폼 로드 완료
        /// </summary>
        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            this.IPBox.Text = "127.0.0.2";
            this.PortBox.Text = "7001";
        }
    } // END OF LoginForm CLASS
}
