﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Configuration;

using UnivChatData;
using UnivChatPacket;

namespace UnivChat {
    /// <summary>
    /// RoomsForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RoomsForm : UserControl {
        internal MainForm mainform;

        public RoomsForm() {
            InitializeComponent();
        }

        /// <summary>
        /// 프로필 버튼
        /// </summary>
        private void Button_Click(object sender, RoutedEventArgs e) {
            if (this.mainform is null) return;

            MainForm mainform = this.mainform;
            Border grid = mainform.MainGrid;

            ProfileForm chatform = new ProfileForm();
            chatform.Name = "ProfileForm";

            grid.Child = chatform;
        }

        /// <summary>
        /// 채널 버튼
        /// </summary>
        private void Button_Click_1(object sender, RoutedEventArgs e) {
            if (this.mainform is null) return;

            RadioButton button = sender as RadioButton;
            if (button is null) return;

            Room room = button.DataContext as Room;
            if (room is null) return;

            ////////////////
            Data data = Data.GetData;

            data.CurrentRoom = room;
            data.Users = room.Users;
            data.Chats = new System.Collections.ObjectModel.ObservableCollection<Chat>();
            ////////////////

            MainForm mainform = this.mainform;

            Border grid = mainform.MainGrid;

            ChatForm chatform = new ChatForm();
            chatform.Name = "ChatForm";

            grid.Child = chatform;
        }

        /// <summary>
        /// 방 추가 버튼 클릭
        /// </summary>
        private void RoomAddButton_Click(object sender, RoutedEventArgs e) {
            RoomCreateForm roomcreate = new RoomCreateForm {
                MainForm = this.mainform,
                Owner = Window.GetWindow(this)
            };

            bool? result = roomcreate.ShowDialog();
            if (result is null) return;

            if (result.Value is true) {
                string roomname = roomcreate.RoomNameBox.Text;

                List<User> users = roomcreate.ReturnUsers;
                users.Add(new User {
                    ID = Data.GetData.Me.ID,
                    Name = Data.GetData.Me.Name,
                    Profile = Data.GetData.Me.Profile,
                    IsOnline = Data.GetData.Me.IsOnline
                });

                string seq = null;
                if (Data.GetData.RoomSequencer.MoveNext()) {
                    seq = Data.GetData.RoomSequencer.Current.ToString();
                }
                if (seq is null) return;

                RoomInfo ar = new RoomInfo {
                    RoomNumber = seq,
                    RoomName = roomname,
                    RoomMasterID = Data.GetData.Me.ID
                };

                Network.GetInstance.SendPacket(ar);

                foreach (User user in users) {
                    AddUserInRoom air = new AddUserInRoom {
                        RoomNumber = seq,
                        AddUserID = user.ID,
                        UserName = user.Name,
                        ProfileText = user.Profile,
                        IsOnline = user.IsOnline
                    };

                    Network.GetInstance.SendPacket(air);
                }
            }
        } // END OF RoomAddButton_Click(object, RoutedEventArgs) Method
    }
}
