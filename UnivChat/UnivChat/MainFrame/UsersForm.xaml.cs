﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using UnivChatData;
using UnivChatPacket;

namespace UnivChat
{
    /// <summary>
    /// UserListForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class UserListForm : UserControl
    {
        public UserListForm()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            ItemsControl userItemsControl = this.UserList;

            userItemsControl.Items.SortDescriptions.Clear();
            userItemsControl.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription("IsOnline", System.ComponentModel.ListSortDirection.Descending)
            );
            userItemsControl.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription("Name", System.ComponentModel.ListSortDirection.Ascending)
            );
        }

        /// <summary>
        /// 유저 추가 버튼 클릭했을 때
        /// </summary>
        private void UserAppendButton_Click(object sender, RoutedEventArgs e) {
            if (Data.GetData.CurrentRoom is null) {
                this.ShowFindFriend();
            }
            else {
                this.ShowUserAppend();
            }
        }

        private void ShowUserAppend() {
            UserAppendForm userappendform = new UserAppendForm();
            userappendform.Owner = Window.GetWindow(this);

            bool? result = userappendform.ShowDialog();

            if (result.Value is true) {
                foreach (User user in userappendform.ReturnUsers) {
                    AddUserInRoom userinroom = new AddUserInRoom() {
                        RoomNumber = Data.GetData.CurrentRoom.RoomNumber,
                        AddUserID = user.ID
                    };

                    System.Diagnostics.Debug.WriteLine(user.ID);
                    Network.GetInstance.SendPacket(userinroom);
                }
            }
        }

        private void ShowFindFriend() {
            FindFriendForm findfriendform = new FindFriendForm();
            findfriendform.Owner = Window.GetWindow(this);

            bool? result = findfriendform.ShowDialog();
            if (result is null) return;

            if (result.Value is true) {
                User user = findfriendform.ResultUser;
                if (user is null) return;

                AddFriendRequest afr = new AddFriendRequest() { FriendID = user.ID, IsMyRequest = true };
                Network.GetInstance.SendPacket(afr);
            }
        }
    }
}
