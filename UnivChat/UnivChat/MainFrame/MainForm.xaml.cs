﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;

using UnivChatData;
using UnivChatPacket;

namespace UnivChat {
    /// <summary>
    /// MainForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainForm : UserControl {
        internal Control maincontrol = null;

        public MainForm() {
            InitializeComponent();
        }

        /// <summary>
        /// 플러스 버튼 눌렀을 때
        /// </summary>
        private void Button_Click(object sender, RoutedEventArgs e) {

        }

        /// <summary>
        /// MainForm 이 로드 되었을 때
        /// </summary>
        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            this.RoomsForm.mainform = this;
        }

        /// <summary>
        /// 프로필 버튼 클릭
        /// </summary>
        private void Button_Click_2(object sender, RoutedEventArgs e) {

        }

        private void Profile_Loaded(object sender, RoutedEventArgs e) {

        }
    }
}
