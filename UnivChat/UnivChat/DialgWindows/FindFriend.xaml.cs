﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using UnivChatData;
using UnivChatPacket;

namespace UnivChat {
    /// <summary>
    /// ListAddWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class FindFriendForm : Window {
        public MainForm MainForm { get; set; }
        public ObservableCollection<User> Users { get; private set; } = new ObservableCollection<User>();
        internal User ResultUser = null;

        public FindFriendForm() {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e) {
            object selected = this.UserListBox.SelectedItem;

            this.ResultUser = selected as User;

            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e) {
            this.DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            this.UserListBox.DataContext = this;
        }

        /// <summary>
        /// 친구찾기 버튼 클릭
        /// </summary>
        private void Find_Click(object sender, RoutedEventArgs e) {
            if (String.IsNullOrWhiteSpace(this.FindBox.Text)) return;

            FindUser user = new FindUser() { UserID = this.FindBox.Text };
            Network.GetInstance.SendPacket(user);

            Task.Run(async () => {
                Packet packet = await Network.GetInstance.WaitToken(typeof(FindUserResponse));

                if (packet is FindUserResponse fur) {
                    User ruser = new User() { ID = fur.UserID, Name = fur.UserName, Profile = fur.ProfileText };
                    this.Users.ClearOnUI();
                    this.Users.AddOnUI(ruser);
                }
            });
        }
    }
}
