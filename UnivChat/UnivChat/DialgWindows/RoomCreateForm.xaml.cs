﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UnivChat {
    /// <summary>
    /// ListAddWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RoomCreateForm : Window {
        public MainForm MainForm { get; set; }
        internal List<UnivChatData.User> ReturnUsers = null;

        public RoomCreateForm() {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {

        }

        private void OKButton_Click(object sender, RoutedEventArgs e) {
            if (String.IsNullOrWhiteSpace(this.RoomNameBox.Text)) return;

            ListBox listbox = this.UserListBox;

            var list = listbox.SelectedItems;
            var users = from object x in list
                        where (x is UnivChatData.User)
                        select (x as UnivChatData.User);
            this.ReturnUsers = new List<UnivChatData.User>(users);

            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e) {
            this.DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {

        }
    }
}
