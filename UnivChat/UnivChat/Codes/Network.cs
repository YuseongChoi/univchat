﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

using System.Threading;

using UnivChatData;
using UnivChatPacket;

using System.IO;

namespace UnivChat {
    static class NetworkHelper {
        public static void AddOnUI<T>(this ICollection<T> collection, T item) {
            Action<T> addMethod = collection.Add;
            // UI 스레드에서 메소드 실행
            System.Windows.Application.Current.Dispatcher.BeginInvoke(addMethod, item);
        }

        public static void ClearOnUI<T>(this ICollection<T> collection) {
            Action clearMethod = collection.Clear;
            System.Windows.Application.Current.Dispatcher.BeginInvoke(clearMethod);
        }
    }

    class Token {
        internal Packet Response { get; set; } = null;

        internal Packet GetResponse(int milisecond) {
            Task task = Task.Run(new Action(this.WaitResponse));

            if (task.Wait(TimeSpan.FromMilliseconds(milisecond))) {
                return this.Response;
            }
            else return null;
        } 

        private void WaitResponse() {
            while (true) {
                if (this.Response != null) {
                    return;
                }
            }
        }
    } // END OF Token CLASS

    class Network {
        static private Network sys = new Network();
        static public Network GetInstance => Network.sys;
        private Network() { }

        ~Network() {
            this.stream?.Close();
            this.server?.Close();
        }

        public string ServerIP { get; set; } = "127.0.0.2";
        public int ServerPort { get; set; } = 7001;

        private TcpClient server = null;
        private NetworkStream stream = null;
        private Dictionary<Type, Token> tokenqueue = new Dictionary<Type, Token>();

        private object locker = new object();

        private string GetLocalIP() {
            IPHostEntry entry = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress adress in entry.AddressList) {
#if DEBUG
                System.Diagnostics.Debug.WriteLine(adress.ToString());
#endif
                if (adress.AddressFamily == AddressFamily.InterNetwork) {
                    return adress.ToString();
                }
            }

            return null;
        }

        public void Connect() {
            if (this.server != null && this.server.Connected) return;

            IPAddress ipaddress = null;
            try {
                ipaddress = IPAddress.Parse(this.ServerIP);
            }
            catch(FormatException exception) {
                throw exception;
            }
            
            IPEndPoint endPoint = new IPEndPoint(ipaddress, this.ServerPort);

            string LocalIP = this.GetLocalIP();
            if (LocalIP is null) {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("인터넷에 연결되어 있지 않습니다.");
#endif
                return;
            }

            this.server = new TcpClient();
            try {
                this.server.Connect(endPoint);
            }
            catch(SocketException exception) {
                throw exception;
            }
            
            if (this.server is null || !this.server.Connected) return;

#if DEBUG
            System.Diagnostics.Debug.WriteLine($"LocalIP : {((IPEndPoint)this.server.Client.LocalEndPoint).ToString()}");
#endif

            this.stream = this.server.GetStream();

            Task.Run(new Action(this.ReceivePacket));
        }

        public void SendPacket(Packet packet) {
            if (this.server is null || this.stream is null) return;

            byte[] send = packet.GetXML();

#if DEBUG
            System.Diagnostics.Debug.WriteLine("-------------Send------------------");
            System.Diagnostics.Debug.WriteLine(packet.GetType());
            System.Diagnostics.Debug.WriteLine(Encoding.UTF8.GetString(send));
            System.Diagnostics.Debug.WriteLine("-----------------------------------");


            if (this.server.Connected) {
                System.Diagnostics.Debug.WriteLine("SERVER CONNECTED");
            }
            else {
                System.Diagnostics.Debug.WriteLine("SERVER DISCONNECTED");
            }
#endif

            Task.Run( () => { this.SendPacket(send); } );
        }

        public void SendPacket(byte[] send) {
            lock(this.locker) {
                try {
                    this.stream.Write(send, 0, send.Length);
                }
                catch(IOException) {
                    return;
                }
                Task task = Task.Delay(20);
                task.Wait();
            }
        }

        public async Task<Packet> WaitToken(Type type) {
            Token token = new Token();
            this.tokenqueue[type] = token;

            Packet packet = await Task.Run<Packet>(() => { return token.GetResponse(5000); });

            return packet;
        }

        private void ReceivePacket() {
            byte[] receive = new byte[this.server.SendBufferSize];
            int count = -1;

            while (!(this.server is null || this.stream is null)) {
                try {
                    count = this.stream.Read(receive, 0, receive.Length);
                }
                catch(IOException) {
                    Environment.Exit(0);
                }
                
                string xml = Encoding.UTF8.GetString(receive, 0, count);
#if DEBUG
                System.Diagnostics.Debug.WriteLine("-------------Received--------------");
                System.Diagnostics.Debug.WriteLine(xml);
                System.Diagnostics.Debug.WriteLine("-----------------------------------");
#endif

                Packet[] packets = Packet.GetPacket(xml);
                if (packets.Length is 0) {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("PARSE FAILED");
#endif
                    continue;
                }

                foreach (Packet packet in packets) {
                    if (this.tokenqueue.ContainsKey(packet.GetType())) {
                        if (this.tokenqueue[packet.GetType()] is null) continue;
                        this.tokenqueue[packet.GetType()].Response = packet;
                        this.tokenqueue[packet.GetType()] = null;
                    }
                    else {
                        this.Handle(packet);
                    }
                }
            } // END OF WHILE
        } // END OF ReceivePacket() Method

        public void Handle(Packet packet) {
            if (packet is null) return;

            // 채팅 추가 메시지
            if (packet is AddChatting addchat) {
                User writeuser = Data.GetData.FindUserFromPool(addchat.WriteUserID);
                Chat chat = new Chat() {
                    User = writeuser,
                    Text = addchat.CharText,
                    Date = addchat.Date
                };

                Room room = Data.GetData.Rooms.FirstOrDefault(r => r.RoomNumber == addchat.RoomNumber);
                if (room is null) return;

                room.AppendLog(chat);

                if (Data.GetData.CurrentRoom != null && Data.GetData.CurrentRoom.RoomNumber == addchat.RoomNumber) {
                    Data.GetData.Chats?.AddOnUI(chat);
                }
            }
            // 방에 유저 추가 메시지
            else if (packet is AddUserInRoom air) {
                Task.Run(() => { this.HandleAddRoom(air); });
            }
            // 친구 추가 요청 메시지
            else if (packet is AddFriendRequest fr) {
                User fruser = Data.GetData.FindUserFromPool(new User() { ID = fr.FriendID, Name = fr.FriendID });
                Data.GetData.FriendsRequests.AddOnUI(fruser);
            }
            // 친구 추가 메시지
            else if (packet is FriendInfo fi) {
                User fiuser = Data.GetData.FindUserFromPool(new User() { ID = fi.FriendID, Name = fi.UserName, Profile = fi.ProfileText });
                Data.GetData.Friends.AddOnUI(fiuser);
            }
            // 프로필 변경 메시지
            else if (packet is ChangeProfile cp) {
                Data.GetData.Me.Profile = cp.UserProfile;
            }
            // 방 추가 메시지
            else if (packet is RoomInfo ri) {
                Room room = Data.GetData.Rooms.FirstOrDefault(r => r.RoomNumber == ri.RoomNumber);
                if (room is null) {
                    room = new Room();
                    room.RoomNumber = ri.RoomNumber;
                    Data.GetData.Rooms.AddOnUI(room);
                }
                room.Name = ri.RoomName;
                room.RoomMaster = Data.GetData.FindUserFromPool(ri.RoomMasterID);
            } // END OF IF
        } // END OF Handle() Method

        public void HandleAddRoom(AddUserInRoom air) {
            Task<Room> task = Task.Run(() => { return this.GetRoom(air.RoomNumber); });

            if (task.Wait(TimeSpan.FromMilliseconds(5000))) {
                Room room = task.Result;

                System.Diagnostics.Debug.WriteLine("AIR : " + room.RoomNumber);

                if (room is null) return;

                User adduser = new User {
                    ID = air.AddUserID,
                    Name = air.UserName,
                    Profile = air.ProfileText,
                    IsOnline = air.IsOnline
                };
                adduser = Data.GetData.FindUserFromPool(adduser);

                User user = room.Users.FirstOrDefault(u => u.ID == air.AddUserID);

                if (user is null) {
                    room.Users.AddOnUI(adduser);
                }

                return;
            }
            else {
                return;
            }
        }

        public Room GetRoom(string roomnumber) {
            Room room = null;
            while (room is null) {
                try {
                    room = Data.GetData.Rooms.FirstOrDefault(r => r.RoomNumber == roomnumber);
                }
                catch (System.InvalidOperationException) {
                    continue;
                }
            }
            return room;
        }
    } // END OF NETWORK CLASS
} // END OF NAMESPACE
