﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;

using UnivChatData;
using UnivChatPacket;

namespace UnivChat {
    public class Data : ChatObject {
        #region Properties
        public User Me {
            get => this.me;
            set {
                this.me = value;
                this.FindUserFromPool(this.me);
                this.OnPropertyChanged(nameof(this.Me));
            }
        }
        public Room CurrentRoom {
            get => this.currentroom;
            set {
                this.currentroom = value;
                this.OnPropertyChanged(nameof(this.CurrentRoom));
            }
        }
        public ObservableCollection<Chat> Chats {
            get => this.chats;
            set {
                this.chats = value;
                this.OnPropertyChanged(nameof(this.Chats));
            }
        }
        public ObservableCollection<User> Users {
            get => this.users;
            set {
                this.users = value;
                this.OnPropertyChanged(nameof(this.Users));
            }
        }
        #endregion

        public static Data GetData => Data.data;
        private static Data data = new Data();
        private Data() {
            this.RoomSequencer = this.GetRoomSequence();
        }

        public ObservableCollection<User> Friends { get; private set; } = new ObservableCollection<User>();
        public ObservableCollection<Room> Rooms { get; private set; } = new ObservableCollection<Room>();
        public ObservableCollection<User> FriendsRequests { get; private set; } = new ObservableCollection<User>();

        public ObservableCollection<User> UserPool { get; private set; } = new ObservableCollection<User>();

        private ObservableCollection<Chat> chats;
        private ObservableCollection<User> users;
        private User me;
        private Room currentroom;

        public IEnumerator<int> RoomSequencer { get; set; } = null;

        private IEnumerator<int> GetRoomSequence() {
            for (int i = 0; i < Int32.MaxValue; i++) {
                yield return i;
            }
        }

        public User FindUserFromPool(string id) {
            User hituser = this.UserPool.FirstOrDefault(u => u.ID == id);
            if (hituser is null) {
                User user = new User() { ID = id };
                this.UserPool.Add(user);

                return user;
            }
            else {
                return hituser;
            }
        }

        public User FindUserFromPool(User user) {
            User hituser = this.UserPool.FirstOrDefault(u => u.ID == user.ID);
            if (hituser is null) {
                this.UserPool.Add(user);

                return user;
            }
            else {
                hituser.Name = user.Name;
                hituser.Profile = user.Profile;
                hituser.IsOnline = user.IsOnline;

                return hituser;
            }
        }

        internal void Reset() {
            this.Friends.ClearOnUI();
            this.Rooms.ClearOnUI();
            this.FriendsRequests.ClearOnUI();

            this.UserPool.ClearOnUI();

            this.Chats = null;
            this.Users = null;
            this.Me = null;
            this.CurrentRoom = null;

            this.RoomSequencer = this.GetRoomSequence();
    }
    } // END OF Data CLASS
} // END OF NAMESPACE
